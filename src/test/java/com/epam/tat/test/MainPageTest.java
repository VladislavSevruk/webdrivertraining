package com.epam.tat.test;

import com.epam.tat.driver.DriverFactory;
import com.epam.tat.page.impl.MainPage;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

/**
 * @author Uladzislau Seuruk.
 */
public class MainPageTest {

    private static final String COMEDY_CENTRAL = "Comedy Central Official Site";

    @Test
    public void pageOpenTest() throws Exception {
        MainPage mainPage = new MainPage(DriverFactory.getChromeDriver());
        mainPage.open();

        Assert.assertTrue(mainPage.getTitle().contains(COMEDY_CENTRAL));
    }

    @Test
    public void socialButtonsTest() throws Exception {
        MainPage mainPage = new MainPage(DriverFactory.getChromeDriver());
        mainPage.open();

        Assert.assertTrue(mainPage.getHeader().isSocialButtonsPresent());
    }

    @AfterTest
    public void tearDownTest() {
        DriverFactory.closeChromeDriver();
    }
}