package com.epam.tat.test;

import com.epam.tat.driver.DriverFactory;
import com.epam.tat.page.impl.FullEpisodePage;
import com.epam.tat.page.impl.MainPage;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

/**
 * @author Uladzislau Seuruk.
 */
public class FullEpisodePageTest {

    @Test
    public void socialButtonsTest() throws Exception {
        MainPage mainPage = new MainPage(DriverFactory.getChromeDriver());
        mainPage.open();
        FullEpisodePage fePage = mainPage.getHeader().clickFullEpisodes();

        Assert.assertTrue(fePage.getHeader().isSocialButtonsPresent());
    }


    @AfterTest
    public void tearDownTest() {
        DriverFactory.closeChromeDriver();
    }
}