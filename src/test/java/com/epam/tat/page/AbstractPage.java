package com.epam.tat.page;

import org.openqa.selenium.WebDriver;

/**
 * @author Uladzislau Seuruk.
 */
public abstract class AbstractPage {

    protected final WebDriver driver;

    public AbstractPage(WebDriver driver) {
        this.driver = driver;
    }

    public abstract void open();
}