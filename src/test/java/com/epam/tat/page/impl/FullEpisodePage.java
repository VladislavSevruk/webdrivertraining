package com.epam.tat.page.impl;

import com.epam.tat.page.AbstractPage;
import com.epam.tat.page.element.impl.PageHeader;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * @author Uladzislau Seuruk.
 */
public class FullEpisodePage extends AbstractPage {

    private static final String PAGE_ADDRESS = "http://www.cc.com/full-episodes";

    @FindBy(css = ".site_header")
    private WebElement header;

    public FullEpisodePage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public PageHeader getHeader() {
        return new PageHeader(driver, header);
    }

    @Override
    public void open() {
        driver.get(PAGE_ADDRESS);
    }
}