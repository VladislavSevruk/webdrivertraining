package com.epam.tat.page.element;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * @author Uladzislau Seuruk.
 */
public class AbstractElement {

    protected final WebDriver driver;
    protected final WebElement element;

    public AbstractElement(WebDriver driver, WebElement element) {
        this.driver = driver;
        this.element = element;
    }
}