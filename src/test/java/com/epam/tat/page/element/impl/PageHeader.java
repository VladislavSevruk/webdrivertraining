package com.epam.tat.page.element.impl;

import com.epam.tat.exception.ElementAbsenceException;
import com.epam.tat.page.element.AbstractElement;
import com.epam.tat.page.impl.FullEpisodePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * @author Uladzislau Seuruk.
 */
public class PageHeader extends AbstractElement {

    @FindBy (css = ".site_menu_wrapper .social>a[href*='facebook.com']")
    private WebElement btnFacebook;

    @FindBy (xpath = "//div[@class='site_menu_wrapper']//a[text()='Full Episodes']")
    private WebElement btnFullEpisodes;

    @FindBy (css = ".site_menu_wrapper .social>a[href*='tumblr.com']")
    private WebElement btnTumblr;

    @FindBy (css = ".site_menu_wrapper .social>a[href*='twitter.com']")
    private WebElement btnTwitter;

    public PageHeader(WebDriver driver, WebElement element) {
        super(driver, element);
        PageFactory.initElements(driver, this);
    }

    public FullEpisodePage clickFullEpisodes() {
        if (!btnFullEpisodes.isDisplayed()) {
            throw new ElementAbsenceException("Full Episodes button is not displayed.");
        }
        btnFullEpisodes.click();
        return new FullEpisodePage(driver);
    }

    public boolean isSocialButtonsPresent() {
        return btnFacebook.isDisplayed()
                && btnTumblr.isDisplayed()
                && btnTwitter.isDisplayed();
    }
}