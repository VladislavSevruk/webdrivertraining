package com.epam.tat.exception;

/**
 * @author Uladzislau Seuruk.
 */
public class ElementAbsenceException extends RuntimeException {

    public ElementAbsenceException() {
        super();
    }

    public ElementAbsenceException(Exception e) {
        super(e);
    }

    public ElementAbsenceException(String message) {
        super(message);
    }

    public ElementAbsenceException(String message, Exception exception) {
        super(message, exception);
    }
}