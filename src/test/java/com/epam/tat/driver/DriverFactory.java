package com.epam.tat.driver;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

/**
 * @author Uladzislau Seuruk.
 */
//TODO: think up about thread-safe realization.
public class DriverFactory {

    private static final String CHROME_DRIVER = "webdriver.chrome.driver";
    private static final String CHROME_PATH = "./driver/chromedriver";
    private static final String FIREFOX_DRIVER = "webdriver.gecko.driver";
    private static final String FIREFOX_PATH = "./driver/geckodriver";

    private static WebDriver chromeDriver;
    private static WebDriver firefoxDriver;

    private DriverFactory() {}

    public static void closeChromeDriver() {
        if (chromeDriver != null) {
            chromeDriver.close();
            chromeDriver = null;
        }
    }

    public static void closeFirefoxDriver() {
        if (firefoxDriver != null) {
            firefoxDriver.close();
            firefoxDriver = null;
        }
    }

    public static WebDriver getChromeDriver() {
        if (chromeDriver == null) {
            System.setProperty(CHROME_DRIVER, CHROME_PATH);
            chromeDriver = new ChromeDriver();
            manageDriver(chromeDriver);
        }
        return chromeDriver;
    }

    public static WebDriver getFirefoxDriver() {
        if (firefoxDriver == null) {
            System.setProperty(FIREFOX_DRIVER, FIREFOX_PATH);
            firefoxDriver = new FirefoxDriver();
            manageDriver(firefoxDriver);
        }
        return firefoxDriver;
    }

    private static void manageDriver(WebDriver driver) {
        driver.manage().timeouts().pageLoadTimeout(120, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.manage().window().maximize();
    }
}